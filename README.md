# Rainguage with UV sensor

The Rain Gauge Station – Model 3424 generates real-time data to help in flood warning, reservoir management, and any other application that calls for timely rainfall information.When there is a chance of flooding, the information from the Rain Gauges helps emergency management experts make decisions that can save both lives and property. These stations are typically used in weather, meteorology, and mesonet applications. The Raingauge station developed using Davis rainguage with a battery-solar powered.

Overexposure to solar ultraviolet (UV) radiation is a risk for public health. Therefore, it is important to provide information to the public about the level of solar UV. The UV-Index (UVI) is the relevant quantity, expressing the erythemally weighted irradiance to a horizontal plane on a simple scale. As solar UV irradiance is strongly variable in time and space, measurements within a network provide the best source of information, provided they can be made available rapidly.
![Raingauge_UV](https://gitlab.com/abdularshadvs/b-l072z-lrwan-rainguage-with-uv-station/-/raw/main/Images/Rainguage%20with%20UV%20sensor.jpeg?ref_type=heads)*Raingauge at ICFOSS, Greenfield Stadium*

# List of Parameters recorded

- Rain measurement
- UV Index measurement
- Every 15 minutes rainfall and UV Index data
- Daily Rainfall data(Transmits every day 8.00 AM)

LoRaWAN based Automatic Rain Gauge Station is a system which measure the Total rainfall and transmits the data at regular intervals.
List of Parameters recorded
The application code is written on top of the I-cube-lrwan package by STM32.

# Prerequisites
    
- STM32 Cube IDE
- [C1_dev_v1.0](https://gitlab.com/icfoss/OpenIoT/c1_dev_v1.0)
- Solar Charge Controller
- Solar Panel (3V, 6V voc)
- [Rain guage Davis](https://www.davisinstruments.com/products/aerocone-rain-collector-with-flat-base-for-vantage-pro2?_pos=3&_sid=2be3af584&_ss=r)
- [UV Sensor Davis](https://www.davisinstruments.com/products/uv-sensor)

## Getting started
 
- Make sure that you have a [C1_dev_v1.0](https://gitlab.com/icfoss/OpenIoT/c1_dev_v1.0) or any STM32 Board.
- Connect the components as shown in the [Wiring Diagram](https://gitlab.com/SruthyS/rainguage-with-uv-sensor/-/blob/main/Hardware/Wiring%20Diagram/uv_wiring%20diagram.pdf)
- Install STM32 Cube IDE [here](https://www.st.com/en/development-tools/stm32cubeide.html#st-get-software)
- Open the tool chain and create a workspace
- Import project to the workspace
- Build project
- Set the Transmission Interval and Activation type (ABP/OTA) in lora_app.h
- If using ABP(Activation by Personalisation) method, Set the dev EUI, device address, network session key and application session key in se-identity.h
- If using OTAA(Over The Air Activation) method, Set the dev EUI, device address, App Key in se-identity.h
- For programming connect C1_dev_v1.0 through B-L072Z-LRWAN1 [here](https://gitlab.com/icfoss/OpenIoT/lorawan_based_rain_gauge_davis/-/blob/master/Hardware/Pgm_conection_STM32board.pdf).
- For programming connect C1_dev_v1.0 through STM32 programmer Module [here](https://gitlab.com/icfoss/OpenIoT/lorawan_based_rain_gauge_davis/-/blob/master/Hardware/Pgm_connection_STMProgrammer.pdf).

# License

This project is licensed under the MIT License - see the LICENSE.md file for details
