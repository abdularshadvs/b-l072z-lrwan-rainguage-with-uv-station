/**
  ******************************************************************************
  * @file	raingauge_station.c
  * @author CDOH Team
  * @brief  Driver for Rain Gauge with UV Station
  *         This file provides firmware functions to manage the following
  *         functionalities
  *           + Initializes necessary pins for the sensors
  *           + Read data from the sensors
  *           + Maps data to the corresponding value range
  *           + Store Necessary data to the EEPROM
  *@verbatim
  ==============================================================================
                        ##### How to use this driver #####
  ==============================================================================
 	 	 	(+) raingaugeStationInit() to Initializes the Rain Gauge station
 	 	 	(+) readAnalogUvValue() to Read UV data from Rain Gauge station
 	 	 	(+) getAccumulatedRainfall() to Read and store Rain data from Rain Gauge station
 	 	 	(+) readBatteryLevel() to Read Battery status
 **/

/**
 * private Includes
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "raingauge_station.h"
#include "adc_if.h"

/**
  * @brief  Write data to memory
  */
static void writeToMemory(uint32_t Address, uint32_t data);

/**
  * @brief  Read data from memory
  */
static uint16_t readFromMemory(uint32_t Address);

/**
  * @brief  Map ADC value
  */
static uint32_t map(uint32_t au32_IN, uint32_t au32_INmin, uint32_t au32_INmax, uint32_t au32_OUTmin, uint32_t au32_OUTmax);

/* user defned variables */

uint16_t rainGaugetipCount = 0;
uint8_t dailyCountFlag = RESET;
uint16_t TotalAccumulatedRainfall=0;


/**
  * @brief  read external battery voltage connected to analog channel 4 through a voltage divider
  * @param  none
  * @retval battery voltage level
  */

uint16_t readBatteryLevel(void) {
	int analogValue = 0;		//ADC reading for battery is stored in this variable
	float batteryVoltage = 0;
	uint16_t batteryLevel = 0;

	enable(BATT_POWER);	//Enable battery voltage reading
	HAL_Delay(10);

	analogValue = ADC_ReadChannels(BATTERY_CHANNEL);	//Read battery voltage reading

	disable(BATT_POWER);	//disable battery voltage reading

	batteryVoltage = (analogValue * 3.3 * ((R1 + R2) / R2)) / 4096;	//battery voltage = ADC value*Vref*2/4096   --12 bit ADC with voltage divider factor of 2

	batteryLevel = (uint16_t) (batteryVoltage * 100);	//Multiplication factor of 100 to convert to int from float

	return batteryLevel;
}

/**
 *
 * @brief initialise a gpio pin in interrupt mode to read every falling edge
 * @note changes in the EXTIx_xx_IRQHandler should be made in order to make the GPIO_Pin act as interrupt
 * @note this pin PA0 is connected to a Rain gauge Read sensor output
 * @param none
 * @retval none
 *
 **/
void rainGaugeInterruptEnable() {
	GPIO_InitTypeDef  GPIO_Init;

	/* Enable GPIO Clock */

	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_SYSCFG_CLK_ENABLE();
    GPIO_Init.Pin = RAINGAUGE_PIN;
	GPIO_Init.Mode = GPIO_MODE_IT_FALLING;
	GPIO_Init.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOB,&GPIO_Init);

	/*Enable and set Rain Interrupt Priority */

	HAL_NVIC_SetPriority((IRQn_Type)EXTI4_15_IRQn,3,0);
    HAL_NVIC_EnableIRQ((IRQn_Type)EXTI4_15_IRQn);
    //APP_PRINTF("Rain_Interrupt_ENable\r\n");
}

/**
 *
 * @brief Increases counter variable to measure Rain fall
 * @param none
 * @retval none
 *
 **/

void rainGaugeTips() {
	rainGaugetipCount++;
}

/**
 *
 * @brief get total accumulated rainfall at transmission interval
 * @note after transmission of rainfall the data is reset and starts new count
 * @param none
 * @retval total rainfall at specified interval (sending only the number of tips, it has to be multiplied by the multiplication factor at the decoding end)
 *
 **/
uint16_t getAccumulatedRainfall() {
	uint16_t rainfall = 0;
	rainfall = rainGaugetipCount;
	TotalAccumulatedRainfall += rainfall;
	rainGaugetipCount = 0;
	return (uint16_t) rainfall;

}

/**
 *
 * @brief calculate accumulated rainfall over a period of time (eg: here we get one day data configured using a downlink from server every morning at 8am)
 * @note if downlink received totalrainfall data is reset and starts new count // also writes to memory location
 * @param none
 * @retval totalrainfall at specified interval (sending only the number of tips, it has to be multiplied by the multiplication factor at the decoding end)
 *
 **/

uint16_t getTotalRainfall(uint8_t downlinkReceived){
	uint16_t TotalRainfall=0;

	if(downlinkReceived == RESET){

		TotalRainfall = TotalAccumulatedRainfall;

	}
	else{
		TotalAccumulatedRainfall=0;
		dailyCountFlag = RESET;
	}
	writeToMemory(RAIN_MEMORY_ADD, (uint16_t)TotalRainfall);
	return (uint16_t) TotalRainfall;
}

/**
 *
 * @brief Initializes the necessary pins before reading sensor data
 * @param none
 * @retval none
 *
 **/

void raingaugeStationGPIO_Init() {
	GPIO_InitTypeDef initStruct = { 0 };
	initStruct.Pull = GPIO_NOPULL;
	initStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	initStruct.Mode = GPIO_MODE_OUTPUT_PP;
	initStruct.Pin = BATT_ENABLE_PIN;

	HAL_GPIO_Init(BATT_ENABLE_PORT, &initStruct);

	initStruct.Pin = UV_ENABLE_PIN;
	HAL_GPIO_Init(UV_ENABLE_PORT, &initStruct);

}

/**
 *
 * @brief manual control of gpio inorder to enable the power to the sensor
 * @param the gpio to be enabled
 * @retval none
 *
 **/
void enable(uint8_t pin) {

	switch (pin) {
		case 1:
			HAL_GPIO_WritePin(BATT_ENABLE_PORT, BATT_ENABLE_PIN, GPIO_PIN_RESET); //for battery
			break;
		case 2:
			HAL_GPIO_WritePin(UV_ENABLE_PORT, UV_ENABLE_PIN, GPIO_PIN_SET); //for UV
			break;
		default:
			break;

}
}

/**
 * @brief manual control of gpio inorder to disable the power to the sensor
 * @param the gpio to be disabled
 * @retval none
 *
 **/
void disable(uint8_t pin) {

	switch (pin) {
		case 1:
			HAL_GPIO_WritePin(BATT_ENABLE_PORT, BATT_ENABLE_PIN, GPIO_PIN_SET); //for battery
			break;
		case 2:
			HAL_GPIO_WritePin(UV_ENABLE_PORT, UV_ENABLE_PIN, GPIO_PIN_RESET); //for UV
			break;
		default:
			break;

}
}

/**
 * @brief Initialize Rain gauge station system
 * @param none
 * @retval none
 *
 **/

void raingaugeStationInit() {

	raingaugeStationGPIO_Init();
	TotalAccumulatedRainfall = readFromMemory(RAIN_MEMORY_ADD);
	writeToMemory(RAIN_MEMORY_ADD, RESET);
}

/**
 *
 * @brief Read Sensor Data
 * @param structure to store the data
 * @retval none
 *
 **/
void readRaingaugeStationParameters(rainfallData_t *sensor_data) {

	sensor_data->batteryLevel = readBatteryLevel();
	sensor_data->rainfall = getAccumulatedRainfall(); // 15 minutes total
	sensor_data->uv = readAnalogUvValue();

}

/**
 *
 * @brief handles writing data to the memory
 * @param Address location of the data to be stored
 * @param data provides the data to be written
 * @retval none
 *
 **/
static void writeToMemory(uint32_t Address, uint32_t data)
{
	/*EEPROM DATA STORING*/

	HAL_FLASHEx_DATAEEPROM_Unlock();
	HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, Address, data);
	HAL_FLASHEx_DATAEEPROM_Lock();

}

/**
 *
 * @brief handles reading data to from memory
 * @param Address location of the data to be retrieved
 * @retval none
 *
 **/

static uint16_t readFromMemory(uint32_t Address){
	return *(uint32_t *)Address;
}

/**
 *
 * @brief  Read UV value connected to analog channel 0
 * @param  none
 * @retval UV value
 *
 **/

uint16_t readAnalogUvValue(void) {

	uint16_t analogValue = 0;
	uint16_t uv = 0;

	analogValue = ADC_ReadChannels(UV_CHANNEL);
	APP_PRINTF("uv value = %d \r\n", analogValue);

	uv = map(analogValue,0,3078,0,16);
	APP_PRINTF("uv index = %d \r\n", uv);

	return uv;
}

/**
 *
 * @brief  map the analog value read
 * @param  ADC Input
 * @param  Min ADC Input value
 * @param  Max ADC Input value
 * @param  Min ADC Output value
 * @param  Max ADC Output value
 * @retval UV value
 *
 **/

static uint32_t map(uint32_t au32_IN, uint32_t au32_INmin, uint32_t au32_INmax, uint32_t au32_OUTmin, uint32_t au32_OUTmax){
	return ((((au32_IN - au32_INmin)*(au32_OUTmax - au32_OUTmin))/(au32_INmax - au32_INmin)) + au32_OUTmin);
}


